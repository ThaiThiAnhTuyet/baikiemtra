﻿namespace BaiKiemTra.Models
{
    public class Transactions
    {
        public int TransactionID { get; set; }
        public int EmployeeID { get; set; }
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public String add_text_here { get; set; }
        public ICollection<Reports> reports { get; set; }

        public ICollection<Logs> logs { get; set; }
    }
}
