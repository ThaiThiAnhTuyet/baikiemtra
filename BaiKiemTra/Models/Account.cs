﻿namespace BaiKiemTra.Models
{
    public class Account
    {
        public int AccountID { get; set; }
        public int CustomerID { get; set; }
        public String Accountname { get; set; }
        public String add_text_here { get; set; }

        public ICollection<Reports> reports { get; set; }

    }
}
