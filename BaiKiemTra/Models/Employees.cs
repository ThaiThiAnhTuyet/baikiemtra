﻿namespace BaiKiemTra.Models
{
    public class Employees
    {
        public int EmployeeID { get; set; }
        public String Firstname { get; set; }
        public String Lastname { get; set; }
        public String contact_and_address { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String add_text_here { get; set; }
        public virtual Transactions Transactions { get; set; }
    }
}
